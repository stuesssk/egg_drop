#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "egg.h"

void
oneEggTest(egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling);
void
twoEggTest (egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling, size_t *floor);

void
manyEggTest(egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling, size_t *floor);
#endif
