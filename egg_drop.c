#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>

#include "egg.h"
#include "functions.h"

int main(int argc, char* argv[])
{
    // Needed for input validation
    char *err;
    errno = 0;

    // You done screwed up if I don't see three comamand line
    // arguments come through
    if (argc != 3 ){
        fprintf(stderr,"Usage: %s <# Floors> <# Eggs>\n", argv[0]);
        return EX_USAGE;
    }

    // Ensure proper input of number of floors
    // Using strtoull and errno techniques for input checking
    // taken from Steven McMasters
    long floors = strtoull(argv[1], &err, 10 );

    // Testing to ensure user gave vaild data for number of Floors
    // Handles any numbers to large to handle or anything that is not just
    //  a number
    if(*err || errno || argv[1][0] == '-')
    {
        printf("Error: Inproper input for number of floors!\n");
        return EX_DATAERR;
    }
    // Handles if user input zero
    if (floors == 0)
    {
        // This is America™ where we start cointing on floor 1
        printf("Error: Number of floors must be greater than or equal"
               " to one(1)!\n");
        return EX_DATAERR;
    }
    // Done ensuring proper data, and will need to make it size_t to
    // pass to other functions.
    size_t numFloors = floors;

    long eggs = strtoull(argv[2], &err, 10 );
    //Testing to ensure user gave valid data for number of floors
    if(*err || errno || argv[2][0] == '-')
    {
        // Handles if strtol is unable to convert number of eggs to a number
        printf("Error: Inproper input for number of eggs!\n");
        return EX_DATAERR;
    }
    // handling if user inputs zero
    if (eggs == 0)
    {
        // Can't have less than 1 egg
        printf("Error: Number of eggs must be greater than or equal"
               " to one(1)!\n");
        return EX_DATAERR;
    }
    // Testing is done, make it a size_t that will be called for later
    size_t numEggs = eggs;

    // Can't drop an egg I don't lay
    egg *testEgg = lay_egg();
    if (!testEgg)
    {
        printf("Unable to lay an EGG‽\n");
        return EX_TEMPFAIL;
    }

    // Intialize the variables I need to carry over all my egg drops
    size_t testFloor = 0;
    size_t drop = 0;
    // Ceiling should always be first known bad floor. intially only bad 
    // floor is 1 above the user input value
    size_t ceiling = numFloors + 1;
    size_t floor = 1;
    size_t whichEgg = 1;


    while(numEggs > 0)
    {
        // Enter switch case depending on the number of eggs remaining
        // to be dropped:
        // --with one egg, linear guessing
        // --with two eggs, steping using formula N(N+1)/2 = M
        //    where N is initial step (decremented by 1 on each drop)
        //    and M is the total number of floors to test
        // --with more than two eggs use binary search as fastest way
        //   to narrow down search window
        printf("Dropping egg %zd from floor:\n", whichEgg++);
        switch (numEggs)
        {
        // only have one egg, be ready to do the leg work of dropping
        // on each floor until it breaks.
        case 1:
            oneEggTest(testEgg, &testFloor, &numEggs, &drop, &ceiling);

            // Last egg had to have broken or I tested every single remaining
            // floor(Max safe floor equals number of floors user input)
            numEggs = 0;

            // Valgrind OverLord commands you destroy your eggs
            destroy_egg(testEgg);
            break;

        // The case with two eggs has beeen solved to find the max safe
        // floor on average in the least number of floors.
        case 2:
            twoEggTest (testEgg, &testFloor, &numEggs, &drop,
                        &ceiling, &floor);
            // Valgrind OverLord commands you destroy your eggs
            destroy_egg(testEgg);
            // Won't create an egg if max safe floor is found
            if (numEggs)
            {
                testEgg = lay_egg();
                if (!testEgg)
                {
                    printf("Unable to lay an EGG‽\n");
                    return EX_TEMPFAIL;
                }
            }
            break;

        // Anything more than two, might as well be infinity
        // at least to the engineer types.
        default:
            manyEggTest (testEgg, &testFloor, &numEggs, &drop,
                         &ceiling, &floor);

            // Valgrind OverLord commands you destroy your eggs
            destroy_egg(testEgg);

            // Won't create an egg if max safe floor is found
            if (numEggs)
            {
                testEgg = lay_egg();
                if (!testEgg)
                {
                    printf("Unable to lay an EGG‽\n");
                    return EX_TEMPFAIL;
                }
            }
            break;
        }
    }
    // Everything executed as intended and we are finished with the program
    return EX_OK;
}
