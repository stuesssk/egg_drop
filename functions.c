#include "functions.h"

void
oneEggTest(egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling)
{
    while(!egg_is_broken(testEgg))
    {
        // Increment floor before testing
        ++(*testFloor);

        // Call teh black box of egg dropping
        egg_drop_from_floor(testEgg, *testFloor);
        (*drop)++;
        printf("#%zd %s\n", *testFloor, 
               (egg_is_broken(testEgg) ? "CRACK" : "safe"));

        // Egg is broken. Max good will be one less than floor on
        if (egg_is_broken(testEgg))
        {
            printf("%zd is the Maximum safe floor, found after %zd drops\n", 
                   (*testFloor - 1), *drop);
            *numEggs = 0;
            break;
        }

        // Ceiling is First know bad floor. If it is the next floor,
        // then you are currently on the maximum height to not 
        // break your egg.
        if (*testFloor == (*ceiling -1))
        {
            printf("%zd is the Maximum safe floor, found after %zd drops\n", 
                   *testFloor, *drop);
            *numEggs = 0;
            break;
        }
    }

}

void
twoEggTest (egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling, size_t *floor)
{
    size_t decrement = 0;
    size_t floorCount = *ceiling - *floor;
    // Solves quaderatic equation to calculate beginning number 
    // to step up by  N(N+1)/2 = M where N is initial step and 
    // M is number of floors
    int step = ceil((-1 + sqrt(8 * floorCount + 1)) / 2);
    while(!egg_is_broken(testEgg))
    {
        // Increment floor testing on each time by step calulated minus
        // number of drops with this egg
        if ((step - decrement) > 1)
        {
            *testFloor += step - decrement++;
        }
        // If calulated step is less than 1, only increase by 1 floor
        else
        {
            (*testFloor)++;
        }
        // Handles if stepping steps past the ceiling
        if (*testFloor >= *ceiling)
        {
            *testFloor = *ceiling - 1;
        }
        // If testing less than floor 1 default to 1st floor
        else if (*testFloor < 1)
        {
           *testFloor = 1;
        }

        // Calling the black box of dropping an egg
        egg_drop_from_floor(testEgg, *testFloor);
        (*drop)++;
        printf("#%zd %s\n", *testFloor, 
               (egg_is_broken(testEgg) ? "CRACK" : "safe"));

        // If egg is broken new ceiling is the tested floor
        if (egg_is_broken(testEgg))
        {
            *ceiling = *testFloor;
            --(*numEggs);
            // Setting your test floor to last good floor
            *testFloor = *floor;
        }
        // Otherwise egg did not break, and the tested floor is the 
        // new global floor
        else
        {
            *floor = *testFloor;
        }

        // We have gone as far as possible if the next floor
        // is the ceiling (ceiling is either beyond the building or
        // a floor the egg will break)
        if (*ceiling - *floor == 1)
        {
            printf("%zd is the Maximum safe floor, found after %zd drops\n", 
                   *floor, *drop);
            *numEggs = 0;
            break;
        }
    }   
}

void
manyEggTest(egg *testEgg, size_t *testFloor, size_t *numEggs,
           size_t *drop, size_t *ceiling, size_t *floor)
{
    while(!egg_is_broken(testEgg))
    {
        size_t floorCount = *ceiling - *floor;
        // Minimum floor increase is 1
        if (floorCount == 1)
        {
            *testFloor += 1;
        }
        //Otherwise use binary search to half the floors
        else
        {
            *testFloor += (floorCount)/2;
        }

        // Minimum floor to test is 1
        if (*testFloor < 1)
        {
            *testFloor = 1;
        }

        // Using black box of egg drop and testing
        egg_drop_from_floor(testEgg, *testFloor);
        (*drop)++;
        printf("#%zd %s\n", *testFloor, 
               (egg_is_broken(testEgg) ? "CRACK" : "safe"));

        // If egg is broken tested floor is the new ceiling,
        // then decrement egg count
        if (egg_is_broken(testEgg))
        {
            *ceiling = *testFloor;
            --(*numEggs);
            // Setting your test floor to last good floor
            *testFloor = *floor;
            // Ensures we din't continue if Max safe floor is found
        }
        // Else you didn't break and that is your new global floor
        else
        {
           *floor = *testFloor;
        }

        // We have gone as far as possible if the next floor
        // is the ceiling (ceiling is either beyond the building or
        // a floor the egg will break)
        if (*ceiling - *floor == 1)
        {
           printf("%zd is the Maximum safe floor, found after %zd drops\n", 
                  *floor, *drop);
           *numEggs = 0;
           break;
        }
    }
}
